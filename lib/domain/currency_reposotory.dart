import 'dart:ffi';

import 'package:currecy_converter_app/data/source/remote/response/currency_response.dart';

abstract class CurrencyRepository {
  Future<List<CurrencyResponse>> getAllData();
  Future<List<CurrencyResponse>>getDataWithDate(DateTime date);
}
