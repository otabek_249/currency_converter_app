import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:flutter/rendering.dart';

class CurrencyDio{
    static  final Dio dio = Dio(BaseOptions(
    baseUrl: 'https://cbu.uz/uz/',
    receiveDataWhenStatusError: true,
    connectTimeout: const Duration(seconds: 30),
    receiveTimeout: const Duration(seconds: 30),
    sendTimeout: const Duration(seconds: 30),
    contentType: 'application/json',
    //headers: {'bearer token' : 'sgddsgfsdf'},
    //queryParameters: {'code' : 2}
  ));

}