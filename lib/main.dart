import 'package:currecy_converter_app/data/source/local/shared_pref.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:currecy_converter_app/ui/home/bloc/home_bloc.dart';
import 'package:currecy_converter_app/ui/home/home_screen.dart';

// Import the colors class
void main() async{
  WidgetsFlutterBinding.ensureInitialized();
  await MyPref.init();
  runApp(const MyApp());

}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Flutter Demo',
      themeMode: ThemeMode.system, // This will automatically switch between light and dark based on system preferences
      theme: _lightTheme, // Light theme
      darkTheme: _darkTheme, // Dark theme
      home: BlocProvider(
        create: (context) => HomeBloc(),
        child: const HomeScreen(),
      ),
    );
  }

  // Define light theme
  ThemeData get _lightTheme {
    return ThemeData(
      primaryColor: AppColors.lightPrimary,
      useMaterial3: true,
      scaffoldBackgroundColor: AppColors.lightPrimary,
      appBarTheme: AppBarTheme(
        backgroundColor: AppColors.lightAccent, // Set background color only
        iconTheme: const IconThemeData(color: AppColors.lightText),
      ),
      textTheme: const TextTheme(
        bodyLarge: TextStyle(color: AppColors.lightText),
        bodyMedium: TextStyle(color: AppColors.lightTextSecondary),
      ),
      colorScheme: ColorScheme.fromSeed(
        seedColor: AppColors.lightPrimary,
        brightness: Brightness.light,
      ).copyWith(background: AppColors.lightPrimary),
    );
  }

  // Define dark theme
  ThemeData get _darkTheme {
    return ThemeData(
      primaryColor: AppColors.darkPrimary,
      useMaterial3: true,
      scaffoldBackgroundColor: AppColors.darkPrimary,
      appBarTheme: AppBarTheme(
        backgroundColor: AppColors.darkAccent, // Set background color only
        iconTheme: IconThemeData(color: AppColors.darkText),
      ),
      textTheme: const TextTheme(
        bodyLarge: TextStyle(color: AppColors.darkText),
        bodyMedium: TextStyle(color: AppColors.darkTextSecondary),
      ),
      colorScheme: ColorScheme.fromSeed(
        seedColor: AppColors.darkPrimary,
        brightness: Brightness.dark,
      ).copyWith(background: AppColors.darkPrimary),
    );
  }
}

class AppColors {
  // Light theme colors
  static const Color lightPrimary = Colors.white; // Light primary color
  static const Color lightAccent = Color(0xFFF1EAEA); // Light accent color
  static const Color lightText = Colors.black;
  static const Color lightTextSecondary = Colors.black;

  // Dark theme colors
  static const Color darkPrimary = Colors.black; // Dark primary color
  static const Color darkAccent = Color(0xF0B0B0B); // Dark accent color
  static const Color darkText = Colors.white;
  static const Color darkTextSecondary = Colors.white;
}
