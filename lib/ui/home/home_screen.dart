import 'package:currecy_converter_app/data/source/local/shared_pref.dart';
import 'package:currecy_converter_app/ui/home/common/currency_item.dart';
import 'package:currecy_converter_app/utils/calculate_dialog.dart';
import 'package:currecy_converter_app/utils/ui_state.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:google_fonts/google_fonts.dart';
import 'bloc/home_bloc.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({super.key});

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  bool isClick = false;
  int chosenId = -1;
  String? _selectedLanguage;
  String ? language;

  final List<String> _languages = ['eng', 'rus',  "o'z"];

  @override
  void initState() {
    context.read<HomeBloc>().add(HomeLoading());
    super.initState();
  }
  void setData ()async {
    _selectedLanguage =await MyPref.getLanguage();
    if(_selectedLanguage=="eng"){
      language ="Valuyuta";
    }
    else if(_selectedLanguage=="o'z"){
      language ="Valuyuta";
    }
    else{
      language ="Валюта";
    }

  }

  void toggleClick() {
    setState(() {
      isClick = !isClick;
    });
  }

  void toggleClick2() {
    setState(() {
      isClick = false;
    });
  }

  Future<void> _onChangeDate(BuildContext context) async {
    final chosenDate = await showDatePicker(
      context: context,
      currentDate: DateTime.now(),
      firstDate: DateTime(2000),
      lastDate: DateTime.now(),
    );

    if (chosenDate == null || !context.mounted) return;
    context.read<HomeBloc>().add(ChangeDate(dateTime: chosenDate));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).primaryColor,
      appBar: AppBar(
        forceMaterialTransparency: true,
        actions: [
          GestureDetector(
            onTap: () {
              _onChangeDate(context);
            },
            child: const Icon(Icons.calendar_month_outlined),
          ),
          const SizedBox(width: 8),
          PopupMenuButton<String>(
            icon: const Icon(Icons.language),
            onSelected: (String newValue) {
              setState(() {
                _selectedLanguage = newValue;
                MyPref.setLanguage(newValue);

                print('Selected language: $_selectedLanguage');
              });
            },
            itemBuilder: (BuildContext context) {
              return _languages.map((String language) {
                return PopupMenuItem<String>(
                  value: language,
                  child: Text(language),
                );
              }).toList();
            },
          ),
          const SizedBox(width: 16),
        ],
        backgroundColor: Colors.transparent,
        title: Text(
          language??"Valuyuta",
          style: GoogleFonts.poppins(
            fontWeight: FontWeight.w600,
            color: Theme.of(context).textTheme.bodyLarge?.color,
          ),
        ),
      ),
      body: BlocConsumer<HomeBloc, HomeState>(
        listener: (context, state) {
          // TODO: implement listener
        },
        builder: (context, state) {
          if (state.uiState == UIState.loading) {
            return const Center(child: CircularProgressIndicator());
          } else if (state.uiState == UIState.success) {
            return ListView.builder(
              itemCount: 65,
              itemBuilder: (_, index) {
                var data = state.currencyList?[index];
                return CurrencyItem(
                  data: state.currencyList![index],
                  index: index,
                  isClick: isClick && index == chosenId,
                  calculationClick: () {
                    showModalBottomSheet(
                      context: context,
                      isScrollControlled: true,
                      builder: (ctx) {
                        return CalculateDialog(
                          ccyName: data!.ccy ?? "",
                          conversionFactor: double.parse(data.rate ?? "1"),
                        );
                      },
                    );
                  },
                  clickIcon: (index) {
                    if (isClick) {
                      chosenId = -1;
                      toggleClick();
                    } else {
                      chosenId = index;
                      toggleClick();
                    }
                  },
                );
              },
            );
          } else {
            return Center(
              child: Text(
                state.error ?? "",
              ),
            );
          }
        },
      ),
    );
  }
}
