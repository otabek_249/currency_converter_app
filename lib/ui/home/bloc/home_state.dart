part of 'home_bloc.dart';


class HomeState {
  UIState? uiState;
  List<CurrencyResponse>? currencyList;
  String? error;
  bool? isClick;
  int ? chooseIndex;
  HomeState({ this.uiState, this.error ,this.currencyList , this.chooseIndex, this.isClick});


  HomeState copyWith({UIState? uiState, List<CurrencyResponse>? currencyList, String? error, bool?isClick, int? chooseIndex }) =>HomeState(
    uiState: uiState??this.uiState, currencyList: currencyList??this.currencyList, error: error??this.error, isClick: isClick??this.isClick, chooseIndex: chooseIndex??this.chooseIndex
  );

}


