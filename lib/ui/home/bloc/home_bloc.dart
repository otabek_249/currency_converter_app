import 'dart:math';

import 'package:bloc/bloc.dart';
import 'package:currecy_converter_app/data/repository/currency_repositroy_impl.dart';
import 'package:currecy_converter_app/data/source/remote/response/currency_response.dart';
import 'package:currecy_converter_app/utils/ui_state.dart';
import 'package:dio/dio.dart';
import 'package:meta/meta.dart';

part 'home_event.dart';
part 'home_state.dart';

class HomeBloc extends Bloc<HomeEvent, HomeState> {
  var repo = CurrencyRepositoryImpl();
  HomeBloc() : super(HomeState()) {
    on<HomeLoading>((event, emit) async {
      emit(HomeState(uiState: UIState.loading));
      try{
        var list = await repo.getAllData();
        emit(HomeState(uiState: UIState.success,currencyList: list));
      }
      on DioException catch(e){
        emit(HomeState(uiState: UIState.fail, error: e.message));
      }
    });
    on<ChangeDate>((event, emit) async {
      emit(HomeState(uiState: UIState.loading));
      try{
        var list = await repo.getDataWithDate(event.dateTime);
        emit(HomeState(uiState: UIState.success,currencyList: list));
      }
      on DioException catch(e){
        emit(HomeState(uiState: UIState.fail, error: e.message));
      }
    });
    on<ConverterClick>((event,emit) async{
      emit(HomeState(isClick: event.isClick,chooseIndex: event.index));
    });
  }
}
