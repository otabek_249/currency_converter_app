part of 'home_bloc.dart';


class HomeEvent {}

final class  HomeLoading extends HomeEvent{}

final class ChangeDate extends HomeEvent{
  DateTime dateTime;
  ChangeDate({ required this.dateTime});
}
final class ConverterClick extends HomeEvent{
  bool isClick;
  int index;
  ConverterClick({ required this.isClick ,required this.index});
}
