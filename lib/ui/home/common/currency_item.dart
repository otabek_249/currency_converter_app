import 'package:cached_network_image/cached_network_image.dart';
import 'package:currecy_converter_app/data/source/local/shared_pref.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:currecy_converter_app/data/source/remote/response/currency_response.dart';

class CurrencyItem extends StatefulWidget {
  final CurrencyResponse data;
  final VoidCallback calculationClick;
  final int index;
  final bool isClick;
  final Function(int index) clickIcon;

  const CurrencyItem({
    super.key,
    required this.clickIcon,
    required this.data,
    required this.index,
    required this.isClick,
    required this.calculationClick,
  });

  @override
  State<CurrencyItem> createState() => _CurrencyItemState();
}

const List<String> countryShortNames = [
  'us', 'eu', 'ru', 'gb', 'jp', 'az', 'bd', 'bg', 'bh', 'bn', 'br', 'by',
  'ca', 'ch', 'cn', 'cu', 'cz', 'dk', 'dz', 'eg', 'af', 'ar', 'ge', 'hk',
  'ke', 'id', 'il', 'iq', 'is', 'jo', 'au', 'kg', 'kh', 'kr', 'kw', 'la',
  'lb', 'ly', 'ma', 'md', 'mg', 'mk', 'mn', 'mu', 'mx', 'my', 'no', 'np',
  'nz', 'ph', 'pk', 'pl', 'qa', 'ro', 'sa', 'se', 'sg', 'lk', 'sy', 'th',
  'tj', 'tm', 'tn', 'tr', 'tz'
];

class _CurrencyItemState extends State<CurrencyItem> {
  late Future<String?> _languageFuture;

  @override
  void initState() {
    super.initState();
    _languageFuture = MyPref.getLanguage();
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder<String?>(
      future: _languageFuture,
      builder: (context, snapshot) {
        if (snapshot.connectionState == ConnectionState.waiting) {
          return Container(
            margin: const EdgeInsets.symmetric(vertical: 8, horizontal: 16),
            height: 70,
            decoration: BoxDecoration(
              color: Theme.of(context).appBarTheme.backgroundColor,
              borderRadius: BorderRadius.circular(12),
            ),
            child: const Center(
              child: CircularProgressIndicator(),
            ),
          );
        } else if (snapshot.hasError) {
          return Container(
            margin: const EdgeInsets.symmetric(vertical: 8, horizontal: 16),
            height: 70,
            decoration: BoxDecoration(
              color: Theme.of(context).appBarTheme.backgroundColor,
              borderRadius: BorderRadius.circular(12),
            ),
            child: Center(
              child: Text('Error: ${snapshot.error}'),
            ),
          );
        } else if (snapshot.hasData) {
          final language = snapshot.data;
          return _buildCurrencyItem(context, language);
        } else {
          return Container(
            margin: const EdgeInsets.symmetric(vertical: 8, horizontal: 16),
            height: 70,
            decoration: BoxDecoration(
              color: Theme.of(context).appBarTheme.backgroundColor,
              borderRadius: BorderRadius.circular(12),
            ),
            child: const Center(
              child: Text('No data'),
            ),
          );
        }
      },
    );
  }

  Widget _buildCurrencyItem(BuildContext context, String? language) {
    bool isDiffPositive = double.tryParse(widget.data.diff ?? "0")! > 0;
    return Container(
      margin: const EdgeInsets.symmetric(vertical: 8, horizontal: 16),
      height: widget.isClick ? 110 : 70,
      decoration: BoxDecoration(
        color: Theme.of(context).appBarTheme.backgroundColor,
        borderRadius: BorderRadius.circular(12),
        boxShadow: [
          BoxShadow(
            color: Colors.grey.withOpacity(0.5),
            offset: const Offset(0, 2),
          ),
        ],
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.end,
        children: [
          Container(
            padding: const EdgeInsets.all(8.0),
            height: 70,
            child: Row(
              children: [
                Container(
                  height: 40,
                  width: 40,
                  padding: const EdgeInsets.all(6),
                  child: CachedNetworkImage(
                    imageUrl: "https://flagcdn.com/w40/${countryShortNames[widget.index]}.png",
                    placeholder: (context, url) => const CircularProgressIndicator(),
                    errorWidget: (context, url, error) => const Icon(Icons.error),
                  ),
                ),
                const SizedBox(width: 8),
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      if (language == "o'z")
                        _buildRow(widget.data.ccyNmUZ, widget.data.diff),
                      if (language == 'rus')
                        _buildRow(widget.data.ccyNmRU, widget.data.diff),
                      if (language == 'eng')
                        _buildRow(widget.data.ccyNmEN, widget.data.diff),
                      const SizedBox(height: 4),
                      _buildRow2(
                          "${widget.data.nominal} ${widget.data.ccy}",
                          "${widget.data.rate} UZS",
                          widget.data.date
                      ),
                    ],
                  ),
                ),
                GestureDetector(
                  onTap: () {
                    setState(() {
                      widget.clickIcon(widget.index);
                    });
                  },
                  child: Icon(
                    widget.isClick ? Icons.keyboard_arrow_up : Icons.keyboard_arrow_down,
                    size: 32,
                  ),
                ),
              ],
            ),
          ),
          Visibility(
            visible: widget.isClick,
            child: Container(
              margin: const EdgeInsets.symmetric(horizontal: 24),
              height: 30,
              width: 120,
              decoration: BoxDecoration(
                color: Colors.grey,
                borderRadius: BorderRadius.circular(8),
              ),
              child: GestureDetector(
                onTap: widget.calculationClick,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Icon(Icons.calendar_month_outlined, color: Theme.of(context).textTheme.bodyLarge?.color),
                    const SizedBox(width: 4),
                    Text(
                      "Hisoblash",
                      style: GoogleFonts.poppins(
                        fontSize: 14,
                        color: Theme.of(context).textTheme.bodyLarge?.color,
                        fontWeight: FontWeight.w600,
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildRow(String? ccy, String? diff) {
    bool isDiffPositive = double.tryParse(diff ?? "0")! > 0;
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 10),
      child: Row(
        children: [
          Expanded(
            child: Text(
              ccy ?? "",
              maxLines: 1,
              overflow: TextOverflow.ellipsis,
              style: GoogleFonts.poppins(
                fontWeight: FontWeight.w500,
                fontSize: 14,
              ),
            ),
          ),
          const SizedBox(width: 10),
          Expanded(
            child: Text(
              isDiffPositive ? "+$diff" : "$diff",
              maxLines: 1,
              overflow: TextOverflow.ellipsis,
              style: GoogleFonts.poppins(
                fontWeight: FontWeight.w500,
                fontSize: 14,
                color: isDiffPositive ? Colors.green : Colors.red,
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildRow2(String? ccy, String? diff, String? date) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 10),
      child: Row(
        children: [
          Flexible(
            child: Text(
              ccy ?? "",
              maxLines: 1,
              overflow: TextOverflow.ellipsis,
              style: GoogleFonts.poppins(
                fontWeight: FontWeight.w500,
                fontSize: 12,
              ),
            ),
          ),
          const SizedBox(width: 10),
          Expanded(
            child: Text(
              diff ?? "",
              maxLines: 1,
              overflow: TextOverflow.ellipsis,
              style: GoogleFonts.poppins(
                fontWeight: FontWeight.w500,
                fontSize: 12,
                color: Theme.of(context).textTheme.bodyLarge?.color,
              ),
            ),
          ),
          const SizedBox(width: 4),
          Container(
            height: 16,
            width: 2,
            color: Colors.black,
          ),
          const SizedBox(width: 4),
          Image.asset(
            "assets/images/calendar.png",
            height: 18,
            width: 18,
          ),
          const SizedBox(width: 4),
          Expanded(
            child: Text(
              date ?? "",
              maxLines: 1,
              overflow: TextOverflow.ellipsis,
              style: GoogleFonts.poppins(
                fontWeight: FontWeight.w500,
                fontSize: 12,
                color: Theme.of(context).textTheme.bodyLarge?.color,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
