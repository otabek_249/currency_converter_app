import 'package:shared_preferences/shared_preferences.dart';

class MyPref {
  static late final SharedPreferences _pref;

  // Initialize SharedPreferences
  static Future<void> init() async {
    _pref = await SharedPreferences.getInstance();
  }

  // Set language in SharedPreferences
  static Future<void> setLanguage(String language) async {
    await _pref.setString("AAA", language);
  }

  // Get language from SharedPreferences
  static Future<String> getLanguage()  async{
    return _pref.getString("AAA") ?? "";
  }
}
