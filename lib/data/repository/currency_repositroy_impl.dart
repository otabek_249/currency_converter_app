import 'package:currecy_converter_app/data/source/remote/response/currency_response.dart';
import 'package:currecy_converter_app/domain/currency_reposotory.dart';
import 'package:currecy_converter_app/service/dio/currency_dio.dart';
import 'package:intl/intl.dart';
import 'package:talker_dio_logger/talker_dio_logger_interceptor.dart';
import 'package:talker_dio_logger/talker_dio_logger_settings.dart';

class CurrencyRepositoryImpl extends CurrencyRepository {
  @override
  Future<List<CurrencyResponse>> getAllData() async {
    try {
      CurrencyDio.dio.interceptors.add(
        TalkerDioLogger(
          settings: const TalkerDioLoggerSettings(
            printRequestHeaders: true,
            printRequestData: true,
            printResponseData: true,
            printResponseHeaders: true,
            printResponseMessage: true,
          ),
        ),
      );
      final response = await CurrencyDio.dio .get('arkhiv-kursov-valyut/json');
      return (response.data as List)
          .map((value) => CurrencyResponse.fromJson(value))
          .toList();
    } catch (e) {
      throw UnimplementedError();
    }
  }

  @override
  Future<List<CurrencyResponse>> getDataWithDate(DateTime date) async {
    try {
      CurrencyDio.dio.interceptors.add(
        TalkerDioLogger(
          settings: const TalkerDioLoggerSettings(
            printRequestHeaders: true,
            printRequestData: true,
            printResponseData: true,
            printResponseHeaders: true,
            printResponseMessage: true,
          ),
        ),
      );
      final response = await CurrencyDio.dio.get('arkhiv-kursov-valyut/json/all/${DateFormat('yyyy-MM-dd').format(date)}/');
      return (response.data as List)
          .map((value) => CurrencyResponse.fromJson(value))
          .toList();
    } catch (e) {
      throw UnimplementedError();
    }
  }


}
