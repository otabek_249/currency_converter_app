import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

class CalculateDialog extends StatefulWidget {
  const CalculateDialog(
      {super.key, required this.ccyName, required this.conversionFactor});

  final String ccyName;
  final double conversionFactor;

  @override
  State<CalculateDialog> createState() => _CalculateDialogState();
}

class _CalculateDialogState extends State<CalculateDialog> {
  final TextEditingController uzsController = TextEditingController();
  final TextEditingController ccyController = TextEditingController();
  bool isUzsToCcy = true;

  @override
  void initState() {
    uzsController.addListener(_uzsConvertListener);
    ccyController.addListener(_ccyConvertListener);
    super.initState();
  }

  @override
  void dispose() {
    uzsController.dispose();
    ccyController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: MediaQuery.of(context).viewInsets + const EdgeInsets.all(12),
      width: double.infinity,
      decoration: BoxDecoration(
        color: Theme.of(context).appBarTheme.backgroundColor,
        borderRadius: Commons.dialogTopBorder,
      ),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          const SizedBox(height: 12),
          Commons.dialogTopSelectView,
          const SizedBox(height: 12),
          TextField(
            cursorColor: Colors.black,
            keyboardType: TextInputType.number,
            controller: isUzsToCcy ? uzsController : ccyController,
            decoration: InputDecoration(
              focusColor: Colors.black,
              labelText: isUzsToCcy ? 'UZS' : widget.ccyName,
              border: const OutlineInputBorder(),
            ),
          ),
          const SizedBox(height: 12),
          TextField(
            readOnly: true,
            controller: isUzsToCcy ? ccyController : uzsController,
            decoration: InputDecoration(
              labelText: isUzsToCcy ? widget.ccyName : 'UZS',
              border: const OutlineInputBorder(),
            ),
          ),
          const SizedBox(height: 12),
          Row(
            children: [
              const Spacer(),
              GestureDetector(
                onTap: () {
                  isUzsToCcy = !isUzsToCcy;
                  setState(() {});
                  print('change calculator $isUzsToCcy');
                },
                child: Container(
                    padding: const EdgeInsets.all(8),
                    decoration: const BoxDecoration(
                      color: Colors.grey,
                      shape: BoxShape.circle,
                    ),
                    child:  const RotatedBox(
                      quarterTurns: 1,
                      child: Icon(
                        Icons.compare_arrows_rounded,
                        color: Colors.white,
                      ),
                    )),
              )
            ],
          )
        ],
      ),
    );
  }

  /* void _uzsConvertListener() {
    if (uzsController.text.isNotEmpty) {
      double uzs = double.tryParse(uzsController.text) ?? 1;
      double ccy = uzs / widget.conversionFactor;
      ccyController.text = ccy.toStringAsFixed(2);
    } else {
      ccyController.clear();
    }
  }

  void _ccyConvertListener() {
    if (ccyController.text.isNotEmpty) {
      double ccy = double.tryParse(ccyController.text) ?? 1;
      double uzs = ccy * widget.conversionFactor;
      uzsController.text = uzs.toStringAsFixed(2);
    } else {
      uzsController.clear();
    }
  }*/

  void _uzsConvertListener() {
    if (uzsController.text.isNotEmpty) {
      double uzs = double.tryParse(uzsController.text) ?? 1;
      double ccy = uzs / widget.conversionFactor;

      ccyController.removeListener(_ccyConvertListener);
      ccyController.text = ccy.toStringAsFixed(2);
      ccyController.addListener(_ccyConvertListener);
    } else {
      ccyController.removeListener(_ccyConvertListener);
      ccyController.clear();
      ccyController.addListener(_ccyConvertListener);
    }
  }

  void _ccyConvertListener() {
    if (ccyController.text.isNotEmpty) {
      double ccy = double.tryParse(ccyController.text) ?? 1;
      double uzs = ccy * widget.conversionFactor;

      uzsController.removeListener(_uzsConvertListener);
      uzsController.text = uzs.toStringAsFixed(2);
      uzsController.addListener(_uzsConvertListener);
    } else {
      uzsController.removeListener(_uzsConvertListener);
      uzsController.clear();
      uzsController.addListener(_uzsConvertListener);
    }
  }
}

class Commons {
  static Radius circular4 = const Radius.circular(4);
  static Radius circular12 = const Radius.circular(12);

  static BorderRadius dialogTopBorder = BorderRadius.only(
    topLeft: Commons.circular12,
    topRight: Commons.circular12,
  );

  static Container dialogTopSelectView = Container(
    alignment: Alignment.center,
    height: 12,
    width: 56,
    padding: const EdgeInsets.symmetric(vertical: 12),
    decoration: BoxDecoration(
      color: Colors.grey,
      borderRadius: BorderRadius.all(
        Commons.circular12,
      ),
    ),
  );
}


String formatCurrency(double amount) {
  // Format the amount with two decimal places
  String formattedAmount = amount.toStringAsFixed(2);

  // Add thousand separators
  final parts = formattedAmount.split('.');
  final integerPart = parts[0];
  final decimalPart = parts[1];

  final buffer = StringBuffer();
  for (int i = integerPart.length - 1; i >= 0; i--) {
    if ((integerPart.length - i) % 3 == 0 && i != integerPart.length - 1) {
      buffer.write(',');
    }
    buffer.write(integerPart[i]);
  }

  // Return the formatted amount with the decimal part
  return '${buffer.toString().split('').reversed.join()}.$decimalPart';
}

